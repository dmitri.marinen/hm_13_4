#include <iostream>
#include "Helpers.h"
#include "Numbers.h"
#include <string>
#include <chrono>
#include <ctime>

int main()
{
    const int rows = 5;
    const int columns = 5;

    int array[rows][columns];

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            array[i][j] = i + j;
        }
    }

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            std::cout << array[i][j];
        }
        std::cout << "\n";
    }

    std::time_t fullDate = std::time(0);
    std::tm* now = std::localtime(&fullDate);
    auto date = (now->tm_mday);

    for (int i = 0; i < rows; i++) {
        int sum = 0;
        int rest = date % rows;
        if (rest == i) {
                for (int j = 0; j < columns; j++) {
                    sum = sum + array[i][j];
            }
                std::cout << sum;
        }
    }

    return 0;
}

void printSum(int a,  int b ) {
    
}
